import {
    Euler,
    EventDispatcher,
    MathUtils,
    Quaternion,
    Vector3,
} from 'three';
import { MAIN_LOOP_EVENTS } from '../../Core/MainLoop.js';

// Note: we could use existing js controls (like
// https://github.com/mrdoob/js/blob/dev/examples/js/controls/FirstPersonControls.js) but
// including these controls in giro3d allows use to integrate them tightly with giro3d.  Especially
// the existing controls are expecting a continuous update loop while we have a pausable one (so our
// controls use .notifyChange when needed)

function limitRotation(camera3D, rot /* , verticalFOV */) {
    // Limit vertical rotation (look up/down) to make sure the user cannot see
    // outside of the cone defined by verticalFOV
    // const limit = MathUtils.degToRad(verticalFOV - camera3D.fov * 0.5) * 0.5;
    const limit = Math.PI * 0.5 - 0.01;
    return MathUtils.clamp(rot, -limit, limit);
}

function applyRotation(instance, camera3D, state) {
    camera3D.quaternion.setFromUnitVectors(
        new Vector3(0, 1, 0), camera3D.up,
    );

    camera3D.rotateY(state.rotateY);
    camera3D.rotateX(state.rotateX);

    instance.notifyChange(instance.camera.camera3D);
}

const MOVEMENTS = {
    38: { method: 'translateZ', sign: -1 }, // FORWARD: up key
    40: { method: 'translateZ', sign: 1 }, // BACKWARD: down key
    37: { method: 'translateX', sign: -1 }, // STRAFE_LEFT: left key
    39: { method: 'translateX', sign: 1 }, // STRAFE_RIGHT: right key
    33: { method: 'translateY', sign: 1 }, // UP: PageUp key
    34: { method: 'translateY', sign: -1 }, // DOWN: PageDown key
};

class FirstPersonControls extends EventDispatcher {
    /**
     * @param {module:Core/Instance~Instance} instance the giro3d instance to control
     * @param {object} options additional options
     * @param {boolean} options.focusOnClick whether or not to focus the renderer domElement on
     * click
     * @param {boolean} options.focusOnMouseOver whether or not to focus when the mouse is over
     * the domElement
     * @param {boolean} options.moveSpeed if > 0, pressing the arrow keys will move the camera
     * @param {number} options.verticalFOV define the max visible vertical angle of the scene in
     * degrees (default 180)
     * @param {number} options.panoramaRatio alternative way to specify the max vertical angle
     * when using a panorama.  You can specify the panorama width/height ratio and the verticalFOV
     * will be computed automatically
     * @param {boolean} options.disableEventListeners if true, the controls will not self listen
     * to mouse/key events.  You'll have to manually forward the events to the appropriate
     * functions: onMouseDown, onMouseMove, onMouseUp, onKeyUp, onKeyDown and onMouseWheel.
     * @param {number} options.minHeight the minimal height of the instance camera
     * @param {number} options.maxHeight the maximal height of the instance camera
     */
    constructor(instance, options = {}) {
        super();
        this.camera = instance.camera.camera3D;
        this.instance = instance;
        this.enabled = true;
        this.moves = new Set();
        if (options.panoramaRatio) {
            const radius = (options.panoramaRatio * 200) / (2 * Math.PI);
            options.verticalFOV = options.panoramaRatio === 2
                ? 180 : MathUtils.radToDeg(2 * Math.atan(200 / (2 * radius)));
        }
        options.verticalFOV = options.verticalFOV || 180;

        options.minHeight = options.minHeight === undefined ? null : options.minHeight;
        options.maxHeight = options.maxHeight === undefined ? null : options.maxHeight;

        // backward or forward move speed in m/s
        options.moveSpeed = options.moveSpeed === undefined ? 10 : options.moveSpeed;
        this.options = options;

        this._isMouseDown = false;
        this._onMouseDownMouseX = 0;
        this._onMouseDownMouseY = 0;

        this._state = {
            rotateX: 0,
            rotateY: 0,
            snapshot() {
                return {
                    rotateX: this.rotateX,
                    rotateY: this.rotateY,
                };
            },
        };
        this.reset();

        const { domElement } = instance.mainLoop.gfxEngine.renderer;
        if (!options.disableEventListeners) {
            domElement.addEventListener('mousedown', this.onMouseDown.bind(this), false);
            domElement.addEventListener('touchstart', this.onMouseDown.bind(this), false);
            domElement.addEventListener('mousemove', this.onMouseMove.bind(this), false);
            domElement.addEventListener('touchmove', this.onMouseMove.bind(this), false);
            domElement.addEventListener('mouseup', this.onMouseUp.bind(this), false);
            domElement.addEventListener('touchend', this.onMouseUp.bind(this), false);
            domElement.addEventListener('keyup', this.onKeyUp.bind(this), true);
            domElement.addEventListener('keydown', this.onKeyDown.bind(this), true);
            domElement.addEventListener('mousewheel', this.onMouseWheel.bind(this), false);
            domElement.addEventListener('DOMMouseScroll', this.onMouseWheel.bind(this), false); // firefox
        }

        this.instance.addFrameRequester(
            MAIN_LOOP_EVENTS.AFTER_CAMERA_UPDATE, this.update.bind(this),
        );

        // focus policy
        if (options.focusOnMouseOver) {
            domElement.addEventListener('mouseover', () => domElement.focus());
        }
        if (options.focusOnClick) {
            domElement.addEventListener('click', () => domElement.focus());
        }
    }

    isUserInteracting() {
        return this.moves.size !== 0 || this._isMouseDown;
    }

    /**
     * Resets the controls internal state to match the camera' state.
     * This must be called when manually modifying the camera's position or rotation.
     *
     * @param {boolean} preserveRotationOnX if true, the look up/down rotation will
     * not be copied from the camera
     */
    reset(preserveRotationOnX = false) {
        // Compute the correct init state, given the calculus in applyRotation:
        // cam.quaternion = q * r
        // => r = invert(q) * cam.quaterion
        // q is the quaternion derived from the up vector
        const q = new Quaternion().setFromUnitVectors(
            new Vector3(0, 1, 0), this.camera.up,
        );
        q.invert();
        // compute r
        const r = this.camera.quaternion.clone().premultiply(q);
        // tranform it to euler
        const e = new Euler(0, 0, 0, 'YXZ').setFromQuaternion(r);

        if (!preserveRotationOnX) {
            this._state.rotateX = e.x;
        }
        this._state.rotateY = e.y;
    }

    /**
     * Updates the camera position / rotation based on occured input events.
     * This is done automatically when needed but can also be done if needed.
     *
     * @param {number} dt ellpased time since last update in seconds
     * @param {boolean} updateLoopRestarted true if giro3d' update loop just restarted
     * @param {boolean} force set to true if you want to force the update, even if it
     * appears unneeded.
     */
    update(dt, updateLoopRestarted, force) {
        if (!this.enabled) {
            return;
        }
        // dt will not be relevant when we just started rendering, we consider a 1-frame move in
        // this case
        if (updateLoopRestarted) {
            dt = 16;
        }

        for (const move of this.moves) {
            if (move.method === 'translateY') {
                this.camera.position.z += (move.sign * this.options.moveSpeed * dt) / 1000;
            } else {
                this.camera[move.method]((move.sign * this.options.moveSpeed * dt) / 1000);
            }
        }

        if (this.options.minHeight !== null
                && this.camera.position.z < this.options.minHeight) {
            this.camera.position.z = this.options.minHeight;
        } else if (this.options.maxHeight !== null
                && this.camera.position.z > this.options.maxHeight) {
            this.camera.position.z = this.options.maxHeight;
        }

        if (this._isMouseDown === true || force === true) {
            applyRotation(this.instance, this.camera, this._state);
        }

        if (this.moves.size) {
            this.instance.notifyChange(this.instance.camera.camera3D);
        }
    }

    // Event callback functions
    // Mouse movement handling
    onMouseDown(event) {
        if (!this.enabled || event.button !== 0) {
            return;
        }
        event.preventDefault();
        this._isMouseDown = true;

        const coords = this.instance.eventToCanvasCoords(event);
        this._onMouseDownMouseX = coords.x;
        this._onMouseDownMouseY = coords.y;

        this._stateOnMouseDown = this._state.snapshot();
    }

    onMouseUp(event) {
        if (!this.enabled || event.button !== 0) {
            return;
        }
        this._isMouseDown = false;
    }

    onMouseMove(event) {
        if (!this.enabled || event.button !== 0) {
            return;
        }
        if (this._isMouseDown === true) {
            // in rigor we have tan(theta) = tan(cameraFOV) * deltaH / H
            // (where deltaH is the vertical amount we moved, and H the renderer height)
            // we loosely approximate tan(x) by x
            const pxToAngleRatio = MathUtils.degToRad(this.camera.fov)
                / this.instance.mainLoop.gfxEngine.height;

            const coords = this.instance.eventToCanvasCoords(event);

            // update state based on pointer movement
            this._state.rotateY = ((coords.x - this._onMouseDownMouseX) * pxToAngleRatio)
                + this._stateOnMouseDown.rotateY;
            this._state.rotateX = limitRotation(
                this.camera,
                ((coords.y - this._onMouseDownMouseY) * pxToAngleRatio)
                    + this._stateOnMouseDown.rotateX,
                this.options.verticalFOV,
            );

            applyRotation(this.instance, this.camera, this._state);
        }
    }

    // Mouse wheel
    onMouseWheel(event) {
        if (!this.enabled) {
            return;
        }
        let delta = 0;
        if (event.wheelDelta !== undefined) {
            delta = -event.wheelDelta;
        // Firefox
        } else if (event.detail !== undefined) {
            delta = event.detail;
        }

        this.camera.fov = MathUtils.clamp(this.camera.fov + Math.sign(delta),
            10,
            Math.min(100, this.options.verticalFOV));

        this.camera.updateProjectionMatrix();

        this._state.rotateX = limitRotation(
            this.camera,
            this._state.rotateX,
            this.options.verticalFOV,
        );

        applyRotation(this.instance, this.camera, this._state);
    }

    // Keyboard handling
    onKeyUp(e) {
        if (!this.enabled) {
            return;
        }
        const move = MOVEMENTS[e.keyCode];
        if (move) {
            this.moves.delete(move);
            this.instance.notifyChange(undefined, false);
            e.preventDefault();
        }
    }

    onKeyDown(e) {
        if (!this.enabled) {
            return;
        }
        const move = MOVEMENTS[e.keyCode];
        if (move) {
            this.moves.add(move);
            this.instance.notifyChange(undefined, false);
            e.preventDefault();
        }
    }
}

export default FirstPersonControls;
